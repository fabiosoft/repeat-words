//
//  main.cpp
//  rip_file
//
//  Created by Fabio Nisci on 28/06/16.
//  Copyright © 2016 Fabio Nisci. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include "Word.hpp"

using namespace std;

void print(vector<Word> v){
    for (unsigned i=0; i< v.size(); i++){
        Word w = v.at(i);
        cout << w.get_word() << ' ' << w.get_counter();
        cout << '\n';
    }

}

vector<Word> get_repeated_words(vector<Word> v){
    vector<Word> list;
    for (unsigned i=0; i< v.size(); i++){
        Word w = v.at(i);
        if (w.get_counter() > 1){
            list.push_back(w);
        }
    }
    return list;
}

int is_in_vector(vector<Word> v, string s){
    for (unsigned i=0; i< v.size(); i++){
        Word w = v.at(i);
        if (w.get_word().compare(s) == 0){
            return i;
        }
    }
    return -1;
}

int main(int argc, const char * argv[]) {
    
    ifstream file("/Users/Fabio/Desktop/rip_file/rip_file/input.txt");
    string line;
    vector<Word> wordlist;

    while (!file.eof()){
        std::getline(file, line);
        
        int index = is_in_vector(wordlist, line);
        if ( index >= 0) {
            Word w = wordlist.at(index);
            w.add(1);
            // update the list
            wordlist.erase(wordlist.begin()+index);
            wordlist.insert(wordlist.begin()+index, w);
            //cout << w.get_word() << " " << w.get_counter() << " trovato!\n";
        }else{
            Word w = Word(line, 1);
            wordlist.push_back(w);
            //cout << w.get_word() << " " << w.get_counter() << " manca! \n";
        }
        
    }
    file.close();
    
    //cout << "---- INPUT LIST ----" << "\n";
    //print(wordlist);
    
    cout << "---- OUTPUT LIST ----" << "\n";
    
    vector<Word>r_wordlist = get_repeated_words(wordlist);
    ofstream outfile("/Users/Fabio/Desktop/rip_file/rip_file/output.txt");
    print(r_wordlist);
    for (unsigned i = 0; i<r_wordlist.size(); i++) {
        Word w = r_wordlist.at(i);
        outfile << w.get_word() << ' ' << w.get_counter() << "\n";
    }
    outfile.close();
    
    return 0;
}
