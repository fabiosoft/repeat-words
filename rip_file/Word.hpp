//
//  Word.hpp
//  rip_file
//
//  Created by Fabio Nisci on 28/06/16.
//  Copyright © 2016 Fabio Nisci. All rights reserved.
//

#ifndef Word_hpp
#define Word_hpp

#include <iostream>
#include <string>

class Word {

    
private:
    int counter;
    std::string word;
public:
    Word(std::string s, int c);
    ~Word();
    std::string get_word();
    void set_word(std::string w);
    int get_counter();
    void set_counter(int c);
    void add(int i);
};



#endif /* Word_hpp */
