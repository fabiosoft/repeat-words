//
//  Word.cpp
//  rip_file
//
//  Created by Fabio Nisci on 28/06/16.
//  Copyright © 2016 Fabio Nisci. All rights reserved.
//

#include "Word.hpp"


Word::Word(std::string s, int c){
    this->word = s;
    this->counter = c;
}
Word::~Word(){}

std::string Word::get_word(){
    return this->word;
}
void Word::set_word(std::string w){
    this->word = w;
}

int Word::get_counter(){
    return this->counter;
}
void Word::set_counter(int c){
    this->counter = c;
}
void Word::add(int i){
    this->counter += i;
}
